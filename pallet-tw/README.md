# 什麼是 Pickupp 的棧板、專車服務?(pallet)

1. 專車服務： 每一台專車可放置2個棧板 一個棧板的尺寸限制為100_120_180 cm 限重：1000 kg 配送時效：當日送達
2. 棧板服務 一個棧板的尺寸限制為100x_120x_180 cm 限重：1000 kg 配送時效：隔日送達
3. 費用與加價服務說明（請見網址）https://drive.google.com/file/d/19ZWkzAe05hxiMNcJZ-loXs9ZUcvTpPVz/view?usp=sharing
4. 備註：不接受裸裝大型物件、精密儀器、傢俱等非箱裝或高單價商品。

您可以依照需求自行選擇需要的服務類型，如果配送時效不是非常緊急，建議使用棧板服務會比較節省成本，另外此二項服務皆須提前安排車輛，故需要於配送的三個工作日前提前下單，謝謝。"
